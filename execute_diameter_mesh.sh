clear
echo "The script starts now"
echo "Hello, $USER!"
n=1

#loop on the value of the diamater D
for D in 1.8817 2.5090 3.7635 5.0180
do
for Mesh in 100 200 300 400 
do
cp -r base FolderInput_${n}
cd inputData
sed -e "s/xxxxxxx/${D}/" -e "s/yyy/${Mesh}/" input.bk >input.data 
cp input.data ../FolderInput_${n}/
cd ../FolderInput_${n}/
cd ../
echo "Replace with a suitable message explaining clearly what is happening in the script"
n=$(( $n + 1 ))
done
done
